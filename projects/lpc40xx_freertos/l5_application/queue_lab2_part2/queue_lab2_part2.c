#include "queue_lab2_part2.h"

void queue__init(queue_s *queue, void *static_memory_for_queue, size_t static_memory_size_in_bytes) {

  queue->curr_count = 0;
  queue->push_index = 0;
  queue->pop_index = 0;
  queue->static_memory_size_in_bytes = static_memory_size_in_bytes;
  queue->static_memory_for_queue = static_memory_for_queue;
}

bool queue__push(queue_s *queue, uint8_t push_value) {

  if (queue->push_index == queue->pop_index &&
      queue->curr_count == (queue->static_memory_size_in_bytes / sizeof(uint8_t)))
    return false;

  queue->static_memory_for_queue[queue->push_index] = push_value;
  queue->push_index = (queue->push_index + 1) % (queue->static_memory_size_in_bytes / sizeof(uint8_t));
  queue->curr_count++;
  return true;
}

bool queue__pop(queue_s *queue, uint8_t *pop_value_ptr) {

  if (queue->push_index == queue->pop_index && queue->curr_count == 0)
    return false;

  *pop_value_ptr = queue->static_memory_for_queue[queue->pop_index];
  queue->pop_index = (queue->pop_index + 1) % (queue->static_memory_size_in_bytes / sizeof(uint8_t));
  queue->curr_count--;
  return true;
}

size_t queue__get_item_count(const queue_s *queue) { return queue->curr_count; }
