#include "queue_lab2.h"

void queue__init(queue_s *queue) {
  // Initialize queue
  // Since the queue is statically defined, we don't need to allocate memory
  // or initialize any additional members
  queue->curr_index = 0;
}

bool queue__push(queue_s *queue, uint8_t push_value) {
  // Check if the queue is full
  // if (queue__get_item_count(queue) >= sizeof(queue->queue_memory) / sizeof(queue->queue_memory[0])) {
  //   return false; // Queue is full
  // }
  if (queue->curr_index < 100) {
    queue->queue_memory[queue->curr_index] = push_value;
    queue->curr_index++;
    return true;
  }
  return false;

  // // Find the next available position in the queue to push the value
  // size_t index = queue__get_item_count(queue);
  // queue->queue_memory[index] = push_value;
  // return true;
}

bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  // Check if the queue is empty
  // if (queue__get_item_count(queue) == 0) {
  //   return false; // Queue is empty
  // }

  // Pop the value from the front of the queue
  // *pop_value = queue->queue_memory[0];

  // // Shift elements in the queue to the left to fill the gap
  // for (size_t i = 0; i < queue__get_item_count(queue) - 1; ++i) {
  //   queue->queue_memory[i] = queue->queue_memory[i + 1];
  // }

  // return true;

  if (queue->curr_index > 0) {
    *pop_value = queue->queue_memory[0];
    for (size_t i = 1; i < queue->curr_index; i++) {
      queue->queue_memory[i - 1] = queue->queue_memory[i];
    }
    queue->curr_index--;
    return true;
  }
  return false;
}

size_t queue__get_item_count(const queue_s *queue) {
  // Calculate the number of items currently in the queue
  // size_t count = 0;
  // while (count < sizeof(queue->queue_memory) / sizeof(queue->queue_memory[0]) && queue->queue_memory[count] != 0) {
  //   ++count;
  // }
  // return count;
  return queue->curr_index;
}
